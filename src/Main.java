import service.SchedulerService;
import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        SchedulerService schedulerService = new SchedulerService();
        schedulerService.scheduler();
    }
}
