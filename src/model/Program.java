package model;

import util.FileUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Program {

    private String name;
    private List<ProgramInstruction> instructions;

    public Program(int index) {

        String path = String.format("programas/%s.txt", index >= 10 ? index : "0" + index);
        File file = new File(path);

        this.readFile(file);
    }

    private void readFile(File file) {

        List<String> lines = FileUtil.readLines(file);

        this.name = lines.get(0);
        this.instructions = new ArrayList<>();

        for (int i = 1; i < lines.size(); i++) {
            String line = lines.get(i);
            this.instructions.add(new ProgramInstruction(line));
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProgramInstruction> getInstructions() {
        return instructions;
    }

    public void setInstructions(List<ProgramInstruction> instructions) {
        this.instructions = instructions;
    }

    @Override
    public String toString() {
        return "Program{" +
                "name='" + name + '\'' +
                ", instructions=" + instructions +
                '}';
    }
}
