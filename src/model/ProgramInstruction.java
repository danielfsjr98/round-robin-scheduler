package model;

public class ProgramInstruction {

    private ProgramInstructionType type;
    private int x;
    private int y;

    public ProgramInstruction(String instruction) {

        this.type = ProgramInstructionType.getFromValue(instruction);

        if (this.type != ProgramInstructionType.ASSIGNMENT) {
            return;
        }

        String[] tokens = instruction.split("=");

        if (tokens[0].equals("X")) {
            this.x = Integer.parseInt(tokens[1]);
        } else {
            this.y = Integer.parseInt(tokens[1]);
        }
    }

    public ProgramInstructionType getType() {
        return type;
    }

    public void setType(ProgramInstructionType type) {
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "ProgramInstruction{" +
                "type=" + type +
                ", x=" + x +
                ", y=" + y +
                '}';
    }
}
