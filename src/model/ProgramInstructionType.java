package model;

public enum ProgramInstructionType {

    ASSIGNMENT   ("=", "Atribuição"),
    IO           ("E/S", "Entrada e Saída"),
    COMMAND      ("COM", "Comando"),
    EXIT         ("SAIDA", "Saída");

    String value;
    String description;

    ProgramInstructionType(String value, String description) {
        this.value       = value;
        this.description = description;
    }

    static ProgramInstructionType getFromValue(String value) {

        for (ProgramInstructionType type : values()) {
            if (value.equals(type.value)) {
                return type;
            }
        }

        return ASSIGNMENT;
    }
}
