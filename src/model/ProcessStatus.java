package model;

public enum ProcessStatus {

    RUNNING ("Executando"),
    READY   ("Pronto"),
    BLOCKED ("Bloqueado"),
    DONE    ("Feito");

    String description;

    ProcessStatus(String description) {
        this.description = description;
    }
}
