package model;

public class BlockProgram {

    private int id;
    private int counter;

    public BlockProgram(int id) {
        this.id = id;
        counter = 3;
    }

    public int getId() {
        return id;
    }

    public int getCounter() {
        return counter;
    }

    public void setCounter(int counter) {
        this.counter = counter;
    }

    @Override
    public String toString() {
        return "model.BlockProgram{" +
                "id=" + id +
                ", counter=" + counter +
                '}';
    }
}
