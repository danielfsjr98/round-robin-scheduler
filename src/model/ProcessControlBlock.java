package model;

public class ProcessControlBlock {

    private Program program;
    private int programCounter;
    private ProcessStatus processStatus;
    private int x;
    private int y;

    public ProcessControlBlock(Program program) {
        this.program        = program;
        this.processStatus  = ProcessStatus.READY;
    }

    public Program getProgram() {
        return program;
    }

    public void setProgram(Program program) {
        this.program = program;
    }

    public int getProgramCounter() {
        return programCounter;
    }

    public void setProgramCounter(int programCounter) {
        this.programCounter = programCounter;
    }

    public ProcessStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessStatus processStatus) {
        this.processStatus = processStatus;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    @Override
    public String toString() {
        return "ProcessControlBlock{" +
                "program=" + program +
                ", processStatus=" + processStatus +
                ", programCounter=" + programCounter +
                '}';
    }
}
