package util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;
import java.util.stream.Collectors;

public class FileUtil {

    public static List<String> readLines(File file) {

        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(file));
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        return br.lines()
                .filter(line -> !line.isBlank())
                .collect(Collectors.toList());
    }
}
