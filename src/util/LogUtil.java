package util;

import java.io.*;

public class LogUtil {

    public static File createFile(int i) {

        String path = "log/log";
        path += i >= 10 ? i : String.format("0%d", i);
        path += ".txt";

        File file = new File(path);

        if (file.exists()) {
            try {
                file.delete();
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        System.out.printf("Arquivo %s criado com sucesso.\n", path);
        return file;
    }

    public static void append(String text, File file) {
        try {
            FileWriter fw = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(fw);
            PrintWriter out = new PrintWriter(bw);

            out.println(text);
            out.close();

            System.out.println(text);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
