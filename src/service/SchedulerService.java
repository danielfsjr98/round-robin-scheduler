package service;

import model.*;
import util.FileUtil;
import util.LogUtil;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class SchedulerService {

    private List<ProcessControlBlock> processTable;
    private List<Integer> readyPrograms;
    private List<BlockProgram> blockPrograms;
    private int quantum;
    private File logFile;

    public SchedulerService() {

        List<ProcessControlBlock> processTable = new ArrayList<>();
        List<Integer> readyPrograms = new ArrayList<>();

        for(int i = 1; i <= 10; i++) {
            Program program = new Program(i);
            processTable.add(new ProcessControlBlock(program));
            readyPrograms.add(i - 1);
        }

        this.processTable  = processTable;
        this.readyPrograms = readyPrograms;
        this.blockPrograms = new ArrayList<>();
        this.quantum       = this.getQuantum();
        this.logFile       = LogUtil.createFile(this.quantum);
    }

    private int getQuantum() {
        File file = new File("programas/quantum.txt");
        List<String> lines = FileUtil.readLines(file);
        return Integer.parseInt(lines.get(0));
    }

    public void scheduler() {

        for (ProcessControlBlock process : processTable) {
            LogUtil.append(String.format("Carregando %s", process.getProgram().getName()), logFile);
        }

        LogUtil.append("", logFile);

        int processChangeCounter = 0;
        int quantumCounter = 0;

        while (!readyPrograms.isEmpty() || !blockPrograms.isEmpty()) {

            if (readyPrograms.isEmpty()) {

                if (blockIsReady()) {
                    addBlockInReady();
                }

                continue;
            }

            int index = readyPrograms.get(0);
            ProcessControlBlock process = processTable.get(index);

            String name = process.getProgram().getName();
            processChangeCounter++;

            LogUtil.append(String.format("Executando %s", name), logFile);

            for(int count = 0; true; count++) {

                int currentCounter = count + process.getProgramCounter();

                if(process.getProcessStatus().equals(ProcessStatus.DONE)) {

                    quantumCounter = quantumCounter + count;

                    if (blockIsReady()) {
                        addBlockInReady();
                    }

                    process.setProgramCounter(currentCounter);

                    LogUtil.append(String.format("Interrompendo %s após %d instrução(ões).", name, count), logFile);
                    LogUtil.append(String.format("%s terminando. X=%d Y=%d", name, process.getX(), process.getY()), logFile);
                    LogUtil.append("", logFile);

                    readyPrograms.remove(0);
                    break;
                }

                if (count == quantum || process.getProcessStatus().equals(ProcessStatus.BLOCKED)) {

                    quantumCounter = quantumCounter + count;

                    if (blockIsReady()) {
                        addBlockInReady();
                    }

                    process.setProgramCounter(currentCounter);

                    if (process.getProcessStatus().equals(ProcessStatus.BLOCKED)) {
                        LogUtil.append(String.format("E/S iniciada em %s", name), logFile);
                    } else {
                        readyPrograms.add(index);
                    }

                    LogUtil.append(String.format("Interrompendo %s após %d instrução(ões).", name, count), logFile);
                    LogUtil.append("", logFile);
                    readyPrograms.remove(0);
                    break;
                }

                ProgramInstruction instruction = process.getProgram().getInstructions().get(currentCounter);

                switch (instruction.getType()) {
                    case ASSIGNMENT -> {
                        if (instruction.getX() != 0) {
                            process.setX(instruction.getX());
                        }

                        if (instruction.getY() != 0) {
                            process.setY(instruction.getY());
                        }
                    }
                    case IO -> {
                        process.setProcessStatus(ProcessStatus.BLOCKED);
                        blockPrograms.add(new BlockProgram(index));
                    }
                    case EXIT -> process.setProcessStatus(ProcessStatus.DONE);
                }
            }
        }

        double averageProcessChange2 = processChangeCounter / (double) processTable.size();
        double averageQuantum2       = quantumCounter / (double) processChangeCounter;

        LogUtil.append(String.format(Locale.ENGLISH, "MÉDIA DE TROCAS: %.2f", averageProcessChange2), logFile);
        LogUtil.append(String.format(Locale.ENGLISH, "MÉDIA DE INSTRUÇÕES: %.2f", averageQuantum2), logFile);
        LogUtil.append(String.format("QUANTUM: %d", quantum), logFile);
    }

    private boolean blockIsReady(){
        if(blockPrograms.size() > 0) decrementBlockPrograms();
        return blockPrograms.size() > 0 && blockPrograms.get(0).getCounter() == 0;
    }

    private void addBlockInReady() {
        blockPrograms.removeIf(program -> {
            if(program.getCounter() == 0) {
                readyPrograms.add(program.getId());
                processTable.get(program.getId()).setProcessStatus(ProcessStatus.READY);
            }
            return program.getCounter() == 0;
        });
    }

    private void decrementBlockPrograms() {
        for (BlockProgram program : blockPrograms) {
            program.setCounter(program.getCounter() - 1);
        }
    }
}
